package InterfazGrafica;

import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.awt.Panel;

import javax.swing.JPanel;

public class CeldaGrafica extends JPanel {
	private Panel panel;
	private Label label;
	private int valor;

	/**
	 * Create the panel.
	 */
	public CeldaGrafica(int x, int y) {
		panel = new Panel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(5 + x, 5 + y, 60, 60);
		panel.setLayout(null);
		
		label = new Label("");
		label.setBackground(new Color(251, 254, 129));
		label.setAlignment(Label.CENTER);
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Dialog", Font.BOLD, 20));
		label.setBounds(5, 5, 50, 50);

		valor = 0;
		
		panel.add(label);
	}

	void setCelda(int valor) {
		this.valor = valor;
		if (this.valor == 0){
			label.setText("");
		}
		else{
			label.setText("" + this.valor);
		}
		colorFondo(this.valor);
		
	}

	Panel getCelda() {
		return panel;
	}

	void colorFondo(int valor) {
		switch (valor) {
		case 0:
			label.setBackground(new Color(251, 254, 129));
			break;
		case 2:
			label.setBackground(new Color(255, 214, 128));
			break;
		case 4:
			label.setBackground(new Color(255, 136, 237));
			break;
		case 8:
			label.setBackground(new Color(255, 128, 128));
			break;
		case 16:
			label.setBackground(new Color(201, 254, 129));
			break;
		case 32:
			label.setBackground(new Color(160, 254, 129));
			break;
		case 64:
			label.setBackground(new Color(128, 255, 169));
			break;
		case 128:
			label.setBackground(new Color(128, 255, 255));
			break;
		case 256:
			label.setBackground(new Color(136, 157, 255));
			break;
		case 512:
			label.setBackground(new Color(136, 136, 255));
			break;
		case 1024:
			label.setBackground(new Color(89, 89, 255));
			break;
		case 2048:
			label.setBackground(new Color(47, 47, 255));
			break;
		}
	}

}
