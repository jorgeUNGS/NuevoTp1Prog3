package InterfazGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Color;
import Negocio.TableroMejorado;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class JuegoGrafica {

	private JFrame frame;
	private TableroGrafico tg;
	private TableroMejorado tm;
	private JLabel puntajeJugador;
	private JLabel nombreJugador;
	private int widthFrame;
	private int heightFrame;
	private JLabel labelVictoria;
	private JLabel labelDerrota;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JuegoGrafica window = new JuegoGrafica();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public JuegoGrafica() {
		initialize(4);

	}

	private void initialize(int cantColumnas) {

		ventanaJuego(cantColumnas);
		mostrar2048();
		mostrarNombre("Jorge");

		tg = new TableroGrafico(cantColumnas);
		tm = new TableroMejorado(cantColumnas);
		frame.getContentPane().add(tg.getTablero());

		labelVictoria(false);
		labelDerrota(false);

		mostrarPuntaje(tm.getPuntaje());

		tm.generarNumero();
		tm.generarNumero();
		tg.setTablero(tm);

		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					moverIzquierda();
					break;
				case KeyEvent.VK_RIGHT:
					moverDerecha();
					break;
				case KeyEvent.VK_DOWN:
					moverAbajo();
					break;
				case KeyEvent.VK_UP:
					moverArriba();
					break;
				}

				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.exit(0);
				}
				if (e.getKeyCode() == KeyEvent.VK_R) {
					reiniciar();
				}
				if (tm.getSeMovieron() == true) {
					tm.generarNumero();
				}
				tm.falsearCorrido();
				setPuntaje(tm.getPuntaje());
				tg.setTablero(tm);

				if (tm.victoria() == true) {
					labelVictoria(true);
					tg.getTablero().setVisible(false);
					if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						System.exit(0);
					}

				}
				if (tm.completo() == true && tm.derrota() == true) {
					labelDerrota(true);
					tg.getTablero().setVisible(false);
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						reiniciar();
						labelDerrota(false);
						
					}
				}
				
			}

			private void reiniciar() {
				tm.reiniciar();
				tm.reiniciarPuntaje();
				tm.generarNumero();
				tm.generarNumero();
				tg.setTablero(tm);
				tg.getTablero().setVisible(true);
			}

		});
	}

	private void moverAbajo() {
		tm.moverDer();
	}

	private void moverArriba() {
		tm.moverIzq();
	}

	private void moverDerecha() {
		tm.moverAba();
	}

	private void moverIzquierda() {
		tm.moverArr();
	}

	private void ventanaJuego(int cantColumnas) {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 102));
		widthFrame = cantColumnas * 60 + 120;
		heightFrame = cantColumnas * 60 + 200;

		frame.getContentPane().setForeground(new Color(255, 255, 102));
		frame.setBounds(500, 150, widthFrame, heightFrame);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private void mostrar2048() {
		frame.getContentPane().setLayout(null);
		JLabel label = new JLabel("2048");
		label.setBounds(10, 14, 100, 50);
		label.setFont(new Font("Tahoma", Font.BOLD, 40));
		frame.getContentPane().add(label);
	}

	private void mostrarNombre(String nombre) {
		nombreJugador = new JLabel("Nombre: " + nombre);
		nombreJugador.setFont(new Font("Arial", Font.BOLD, 15));
		nombreJugador.setBounds(193, 14, 141, 25);
		frame.getContentPane().add(nombreJugador);
	}

	private void mostrarPuntaje(int puntaje) {
		puntajeJugador = new JLabel("Puntaje: " + puntaje);
		puntajeJugador.setFont(new Font("Arial", Font.BOLD, 15));
		puntajeJugador.setBounds(193, 39, 141, 25);
		frame.getContentPane().add(puntajeJugador);
	}

	private void labelDerrota(boolean b) {
		labelDerrota = new JLabel("Derrota!");
		labelDerrota.setBounds(84, 140, 190, 55);
		frame.getContentPane().add(labelDerrota);
		labelDerrota.setForeground(new Color(0, 0, 0));
		labelDerrota.setBackground(new Color(0, 51, 204));
		labelDerrota.setHorizontalAlignment(SwingConstants.CENTER);
		labelDerrota.setFont(new Font("Tahoma", Font.BOLD, 45));
		labelDerrota.setVisible(b);
	}

	private void labelVictoria(boolean b) {
		labelVictoria = new JLabel("Victoria!");
		labelVictoria.setBounds(67, 140, 213, 55);
		frame.getContentPane().add(labelVictoria);
		labelVictoria.setForeground(new Color(0, 0, 0));
		labelVictoria.setBackground(new Color(0, 51, 204));
		labelVictoria.setHorizontalAlignment(SwingConstants.CENTER);
		labelVictoria.setFont(new Font("Tahoma", Font.BOLD, 45));
		labelVictoria.setVisible(b);
	}

	private void bttnReiniciar() {
		JButton bttnReiniciar = new JButton("Reiniciar");
		bttnReiniciar.setBackground(new Color(255, 153, 102));
		bttnReiniciar.setFont(new Font("Arial", Font.BOLD, 20));
		frame.getContentPane().add(bttnReiniciar);
		bttnReiniciar.setBounds(112, 220, 129, 39);
		bttnReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				labelDerrota(false);
				labelVictoria(false);
				tm.reiniciar();
				tm.reiniciarPuntaje();
				tm.generarNumero();
				tm.generarNumero();
				tg.setTablero(tm);
				tg.getTablero().setVisible(true);
			}
		});
	}

	private void setPuntaje(int puntaje) {
		puntajeJugador.setText("Puntaje: " + puntaje);
	}
}
