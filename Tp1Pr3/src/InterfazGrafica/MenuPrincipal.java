package InterfazGrafica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.awt.CardLayout;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import Negocio.TableroMejorado;

import javax.swing.ImageIcon;

public class MenuPrincipal {

	private JFrame frame;
	private TableroGrafico tg;
	private TableroMejorado tm;
	private JLabel puntajeJugador;
	private JLabel nombreJugador;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal window = new MenuPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuPrincipal() {
		initialize();
	}

	private void initialize() {

		int alto = 600;
		int ancho = 600;

		constructorFrame(alto, ancho);

		final JPanel panelPrincipal = new JPanel();
		panelPrincipal.setBackground(new Color(100, 149, 237));
		frame.getContentPane().add(panelPrincipal);
		panelPrincipal.setLayout(null);

		final JPanel panelClasico = new JPanel();
		panelClasico.setBackground(new Color(100, 149, 237));
		frame.getContentPane().add(panelClasico);
		panelClasico.setLayout(null);
		JLabel label = new JLabel("2048");
		label.setBounds(10, 11, 76, 39);
		label.setFont(new Font("Tahoma", Font.BOLD, 30));
		panelClasico.add(label);
		nombreJugador = new JLabel("Nombre: " + "jorge");
		nombreJugador.setBounds(185, 25, 100, 14);
		panelClasico.add(nombreJugador);
		
		tg = new TableroGrafico(4);
		tm = new TableroMejorado(4);
		panelClasico.add(tg);
		tg.setVisible(true);
		tg.getTablero().setVisible(true);
		panelClasico.getRootPane().add(tg);
		frame.getContentPane().add(tg);

		
		
		final JPanel panelDesafio = new JPanel();
		panelDesafio.setBackground(new Color(100, 149, 237));
		frame.getContentPane().add(panelDesafio);
		panelDesafio.setLayout(null);

		
		
		
		labelMenuPrincipal(panelPrincipal);

		buttonsMenuPrincipal(panelPrincipal, panelClasico, panelDesafio);

		checkSonido(panelPrincipal);

		volverMenuPrincipal(panelPrincipal, panelDesafio);

		labelMenuDesafio(panelDesafio);

		buttonsModoDesafio(panelDesafio);

		checkSonido(panelDesafio);

	}

	private void moverAbajo() {
		tm.moverDer();
		tm.generarNumero();
		tm.falsearCorrido();
		setPuntaje(tm.getPuntaje());
		tg.setTablero(tm);
	}

	private void moverArriba() {
		tm.moverIzq();
		tm.generarNumero();
		tm.falsearCorrido();
		setPuntaje(tm.getPuntaje());
		tg.setTablero(tm);
	}

	private void moverDerecha() {
		tm.moverAba();
		tm.generarNumero();
		tm.falsearCorrido();
		setPuntaje(tm.getPuntaje());
		tg.setTablero(tm);
	}

	private void moverIzquierda() {
		tm.moverArr();
		tm.generarNumero();
		tm.falsearCorrido();
		setPuntaje(tm.getPuntaje());
		tg.setTablero(tm);
	}



	private void mostrarPuntaje(int puntaje) {
		puntajeJugador = new JLabel("Puntaje: " + puntaje);
		puntajeJugador.setBounds(185, 50, 100, 14);
		frame.getContentPane().add(puntajeJugador);
	}

	private void setPuntaje(int puntaje) {
		puntajeJugador.setText("Puntaje: " + puntaje);
	}

	private void constructorFrame(int alto, int ancho) {
		frame = new JFrame();
		frame.getContentPane().setForeground(new Color(0, 0, 0));
		frame.getContentPane().setFont(new Font("Arial Narrow", Font.BOLD, 14));
		frame.setBackground(SystemColor.inactiveCaption);
		frame.getContentPane().setBackground(new Color(222, 184, 135));
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		frame.setSize(alto, ancho);
		frame.setResizable(false);
	}

	private void buttonsMenuPrincipal(final JPanel panelPrincipal, final JPanel panelClasico, final JPanel panelDesafio) {
		JButton btnModoClasico = new JButton("Modo cl\u00E1sico\r\n");
		btnModoClasico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelPrincipal.setVisible(false);
				panelClasico.setVisible(true);
			}
		});
		btnModoClasico.setForeground(new Color(0, 0, 0));
		btnModoClasico.setFont(new Font("Tahoma", Font.PLAIN, 27));
		btnModoClasico.setBackground(new Color(100, 149, 237));
		btnModoClasico.setBounds(200, 200, 200, 100);
		panelPrincipal.add(btnModoClasico);

		JButton btnModoDesafio = new JButton("Desaf\u00EDo");
		btnModoDesafio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelPrincipal.setVisible(false);
				panelDesafio.setVisible(true);
			}
		});
		btnModoDesafio.setForeground(new Color(0, 0, 0));
		btnModoDesafio.setFont(new Font("Tahoma", Font.PLAIN, 27));
		btnModoDesafio.setBackground(new Color(100, 149, 237));
		btnModoDesafio.setBounds(200, 315, 200, 100);
		panelPrincipal.add(btnModoDesafio);
	}

	private void volverMenuPrincipal(final JPanel panelVolver, final JPanel panelActual) {
		JButton volver = new JButton("");
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelVolver.setVisible(true);
				panelActual.setVisible(false);
			}
		});
		volver.setIcon(
				new ImageIcon(MenuPrincipal.class.getResource("/com/sun/javafx/scene/web/skin/Undo_16x16_JFX.png")));
		volver.setSelectedIcon(
				new ImageIcon(MenuPrincipal.class.getResource("/com/sun/javafx/scene/web/skin/Undo_16x16_JFX.png")));
		volver.setFont(new Font("Tahoma", Font.PLAIN, 25));
		volver.setBackground(new Color(100, 149, 237));
		volver.setBounds(15, 15, 30, 30);
		panelActual.add(volver);
	}

	private void checkSonido(final JPanel panelMenu) {
		JCheckBox checkSonidos = new JCheckBox("MUTE");
		checkSonidos.setBackground(new Color(100, 149, 237));
		checkSonidos.setBounds(520, 10, 97, 23);
		panelMenu.add(checkSonidos);

	}

	private void buttonsModoDesafio(final JPanel panelDesafio) {
		JButton btn_x3 = new JButton("3X3");
		btn_x3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x3.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x3.setBackground(new Color(100, 149, 237));
		btn_x3.setBounds(110, 204, 100, 50);
		panelDesafio.add(btn_x3);

		JButton btn_x4 = new JButton("4X4");
		btn_x4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x4.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x4.setBackground(new Color(100, 149, 237));
		btn_x4.setBounds(250, 204, 100, 50);
		panelDesafio.add(btn_x4);

		JButton btn_x5 = new JButton("5X5");
		btn_x5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x5.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x5.setBackground(new Color(100, 149, 237));
		btn_x5.setBounds(382, 204, 100, 50);
		panelDesafio.add(btn_x5);

		JButton btn_x6 = new JButton("6X6");
		btn_x6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x6.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x6.setBackground(new Color(100, 149, 237));
		btn_x6.setBounds(110, 279, 100, 50);
		panelDesafio.add(btn_x6);

		JButton btn_x7 = new JButton("7X7");
		btn_x7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x7.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x7.setBackground(new Color(100, 149, 237));
		btn_x7.setBounds(250, 279, 100, 50);
		panelDesafio.add(btn_x7);

		JButton btn_x8 = new JButton("8X8");
		btn_x8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x8.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x8.setBackground(new Color(100, 149, 237));
		btn_x8.setBounds(382, 279, 100, 50);
		panelDesafio.add(btn_x8);

		JButton btn_x9 = new JButton("9X9");
		btn_x9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn_x9.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_x9.setBackground(new Color(100, 149, 237));
		btn_x9.setBounds(182, 352, 100, 50);
		panelDesafio.add(btn_x9);

		JButton btn_x10 = new JButton("10X10");
		btn_x10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn_x10.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btn_x10.setBackground(new Color(100, 149, 237));
		btn_x10.setBounds(320, 354, 100, 50);
		panelDesafio.add(btn_x10);
	}

	private void labelMenuDesafio(final JPanel panelOpc) {
		JLabel tituloModoDesafio = new JLabel("MODO DESAFIO");
		tituloModoDesafio.setHorizontalAlignment(SwingConstants.CENTER);
		tituloModoDesafio.setFont(new Font("Tahoma", Font.BOLD, 35));
		tituloModoDesafio.setBackground(new Color(100, 149, 237));
		tituloModoDesafio.setBounds(101, 48, 400, 83);
		panelOpc.add(tituloModoDesafio);
		JLabel mensajeDesafio = new JLabel("Te desafiamos a que ganes en los distintos tableros");
		mensajeDesafio.setFont(new Font("Tahoma", Font.PLAIN, 15));
		mensajeDesafio.setHorizontalAlignment(SwingConstants.CENTER);
		mensajeDesafio.setBounds(110, 142, 372, 35);
		panelOpc.add(mensajeDesafio);
	}

	private void labelMenuPrincipal(final JPanel panelMenu) {
		JLabel titulo = new JLabel("JUEGO2048\r\n");
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setFont(new Font("Tahoma", Font.BOLD, 45));
		titulo.setBounds(160, 89, 282, 100);
		panelMenu.add(titulo);

		JLabel lblUniversidadNacionalGeneral = new JLabel("Universidad Nacional General Sarmiento ");
		lblUniversidadNacionalGeneral.setBounds(200, 484, 282, 20);
		panelMenu.add(lblUniversidadNacionalGeneral);

		JLabel lblTrabajoPracticoN = new JLabel("Programaci\u00F3n III - Trabajo Pr\u00E1ctico n\u00B01");
		lblTrabajoPracticoN.setBounds(200, 515, 250, 20);
		panelMenu.add(lblTrabajoPracticoN);

		JLabel lblJorgeRearteCarvalho = new JLabel("Jorge Rearte Carvalho - Jeremias Medrano");
		lblJorgeRearteCarvalho.setBounds(200, 546, 305, 20);
		panelMenu.add(lblJorgeRearteCarvalho);
	}

	public static void musicaFondo(Object menuInicial)
			throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		java.net.URL url = menuInicial.getClass().getResource("/sonidos/cancionjuego.mp3");
		AudioInputStream sound = AudioSystem.getAudioInputStream(url);
		Clip clip = AudioSystem.getClip();
		clip.open(sound);
		clip.start();
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	public static void musicaClick(JFrame i)
			throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		java.net.URL url = i.getClass().getResource("/sonidos/click.wav");
		AudioInputStream sound = AudioSystem.getAudioInputStream(url);
		Clip clip = AudioSystem.getClip();
		clip.open(sound);
		clip.start();
	}
}
