package InterfazGrafica;

import java.awt.Color;
import java.awt.Font;
import java.awt.Panel;
import javax.swing.JPanel;

import Negocio.TableroMejorado;


public class TableroGrafico extends JPanel {
	private JPanel panel;
	private int cantColumnas;
	private CeldaGrafica[][] matriz;

	public TableroGrafico(int cantColumnas) {

		this.cantColumnas = cantColumnas;
		panel = new JPanel();
//		panel.setFont(new Font("Arial Black", Font.BOLD, 20));
		panel.setBackground(Color.BLACK);
		panel.setBounds(50, 100, cantColumnas * 60 + 10, cantColumnas * 60 + 10);
		panel.setLayout(null);
		matriz = new CeldaGrafica[cantColumnas][cantColumnas];

		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				matriz[i][j] = new CeldaGrafica(i * 60, j * 60);
				panel.add(matriz[i][j].getCelda());
			}
		}
	}

	JPanel getTablero() {
		return panel;
	}
	void setTablero(TableroMejorado tm){
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				matriz[i][j].setCelda(tm.getCelda(i, j).getValor());
			}
		}
	}

}
