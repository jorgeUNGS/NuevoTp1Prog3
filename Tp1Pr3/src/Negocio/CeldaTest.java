package Negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class CeldaTest {

	@Test
	public void testCelda() {
		Celda c = new Celda();
		
		c.setValor(4);
		
		assertTrue(c.getValor() == 4);
		assertFalse(c.getValor() == 0);
		
		c.setValor(0);
		
		assertTrue(c.getValor() == 0);
		assertFalse(c.getValor() == 4);
	}

}
