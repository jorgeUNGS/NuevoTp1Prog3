package Negocio;

public class Puntaje {
	private int puntaje;

	void sumarPuntaje(int valor) {
		this.puntaje += valor;
	}

	int getPuntaje() {
		return this.puntaje;
	}
	
	void setPuntaje(int valor){
		puntaje = valor;
	}
}
