package Negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class PuntajeTest {

	@Test
	public void test() {
		Puntaje p = new Puntaje();
		
		assertTrue(p.getPuntaje() == 0);
		p.sumarPuntaje(3);
		p.sumarPuntaje(7);
		assertTrue(p.getPuntaje() == 10);
		
	}

}
