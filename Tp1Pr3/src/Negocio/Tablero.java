package Negocio;

public class Tablero {

	protected Celda[][] tablero;
	protected int cantCeldas;
	protected int cantColumnas;

	int getCantCeldas() {
		return cantCeldas;
	}

	int getCantColumnas() {
		return cantColumnas;
	}

	public Celda getCelda(int i, int j) {
		return tablero[i][j];
	}

	void setCelda(int i, int j, int valor) {
		tablero[i][j].setValor(valor);
	}

	public String toString() {
		String ret = "";
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				ret += "" + tablero[i][j].getValor() + " ";
			}
			ret += "\n";
		}
		return ret;
	}

	public boolean victoria() {
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				if (tablero[i][j].getValor() == 2048) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean derrota() {
		for (int i = 0; i < cantColumnas - 1; i++) {
			for (int j = 0; j < cantColumnas - 1; j++) {
				if ((tablero[i][j].getValor() == tablero[i+1][j].getValor())
						|| (tablero[i][j].getValor() == tablero[i][j+1].getValor())) {
					return false;
				}
			}
		}
		for (int i = 0; i < cantColumnas - 1; i++) {
			if ((tablero[i][cantColumnas - 1].getValor() == tablero[i+1][cantColumnas - 1].getValor())
					|| (tablero[cantColumnas - 1][i].getValor() == tablero[cantColumnas - 1][i+1].getValor())) {
				return false;
			}
		}
		return true;
	}

	public boolean completo() {
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				if (getCelda(i, j).getValor() == 0) {
					return false;
				}
			}
		}
		return true;
	}
	public void reiniciar(){
		
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				tablero[i][j].setValor(0);
			}
		}
	}
}
