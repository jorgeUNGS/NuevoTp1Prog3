package Negocio;

import java.util.Random;
import java.util.Scanner;

public class TableroMejorado extends Tablero {

	private Puntaje puntaje;
	private Random random;
	private boolean seMovieron;

	public TableroMejorado(int cantColumnas) {
		if (cantColumnas < 2) {
			throw new NullPointerException("La cantidad de columnas debe ser mayor a 1. Valor: " + cantColumnas);
		} else {
			this.seMovieron = false;
			this.cantColumnas = cantColumnas;
			this.cantCeldas = cantColumnas * cantColumnas;
			this.tablero = new Celda[cantColumnas][cantColumnas];
			this.puntaje = new Puntaje();
			this.random = new Random();
			for (int i = 0; i < cantColumnas; i++) {
				for (int j = 0; j < cantColumnas; j++) {
					tablero[i][j] = new Celda();
					tablero[i][j].setValor(0);
				}
			}
		}
	}

	public int getPuntaje() {
		return puntaje.getPuntaje();
	}

	public boolean getSeMovieron() {
		return seMovieron;
	}
	public void reiniciarPuntaje(){
		puntaje.setPuntaje(0);;
	}
	public void generarNumero() {
		int i = random.nextInt(getCantColumnas());
		int j = random.nextInt(getCantColumnas());
		int aux = random.nextInt(4);
		while (getCelda(i, j).getValor() != 0) {
			i = random.nextInt(getCantColumnas());
			j = random.nextInt(getCantColumnas());
		}
		if (aux != 0)
			setCelda(i, j, 2);
		else
			setCelda(i, j, 4);
	}

	public void falsearCorrido() {
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				tablero[i][j].setCorrido(false);
			}
		}
		seMovieron = false;
	}

	public void moverArr() {
		for (int i = 1; i < cantColumnas; i++) {
			for (int j = 0; j < cantColumnas; j++) {
				if (tablero[i][j].getValor() != 0) {
					auxMoverArr(i, j);
				}
			}
		}
	}

	public void auxMoverArr(int i, int j) {
		if (i - 1 >= 0) {
			if (tablero[i - 1][j].getValor() == 0) {
				moverValor(tablero[i][j], tablero[i - 1][j]);
				auxMoverArr(i - 1, j);
				seMovieron = true;
			} else {
				if ((tablero[i][j].getValor() == tablero[i - 1][j].getValor())
						&& (tablero[i - 1][j].isCorrido() == false)) {
					tablero[i - 1][j].setCorrido(true);
					sumarPuntaje(tablero[i][j], tablero[i - 1][j]);
					moverValor(tablero[i][j], tablero[i - 1][j]);
					seMovieron = true;
				}

			}
		}
	}

	public void moverAba() {
		for (int i = cantColumnas - 2; i >= 0; i--) {
			for (int j = 0; j < cantColumnas; j++) {
				if (tablero[i][j].getValor() != 0) {
					auxMoverAba(i, j);
				}
			}
		}
	}

	private void auxMoverAba(int i, int j) {
		if (i + 1 < cantColumnas) {
			if (tablero[i + 1][j].getValor() == 0) {
				moverValor(tablero[i][j], tablero[i + 1][j]);
				auxMoverAba(i + 1, j);
				seMovieron = true;
			} else {
				if ((tablero[i][j].getValor() == tablero[i + 1][j].getValor())
						&& (tablero[i + 1][j].isCorrido() == false)) {
					tablero[i + 1][j].setCorrido(true);
					sumarPuntaje(tablero[i][j], tablero[i + 1][j]);
					moverValor(tablero[i][j], tablero[i + 1][j]);
					seMovieron = true;
				}
			}
		}
	}

	public void moverIzq() {
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = 1; j < cantColumnas; j++) {
				if (tablero[i][j].getValor() != 0) {
					auxMoverIzq(i, j);
				}
			}
		}
	}

	private void auxMoverIzq(int i, int j) {
		if (j - 1 >= 0) {
			if (tablero[i][j - 1].getValor() == 0) {
				moverValor(tablero[i][j], tablero[i][j - 1]);
				auxMoverIzq(i, j - 1);
				seMovieron = true;
			} else {
				if ((tablero[i][j].getValor() == tablero[i][j - 1].getValor())
						&& (tablero[i][j - 1].isCorrido() == false)) {
					tablero[i][j - 1].setCorrido(true);
					sumarPuntaje(tablero[i][j], tablero[i][j - 1]);
					moverValor(tablero[i][j], tablero[i][j - 1]);
					seMovieron = true;
				}
			}
		}
	}

	public void moverDer() {
		for (int i = 0; i < cantColumnas; i++) {
			for (int j = cantColumnas - 2; j >= 0; j--) {
				if (tablero[i][j].getValor() != 0) {
					auxMoverDer(i, j);
				}
			}
		}
	}

	private void auxMoverDer(int i, int j) {
		if (j + 1 < cantColumnas) {
			if (tablero[i][j + 1].getValor() == 0) {
				moverValor(tablero[i][j], tablero[i][j + 1]);
				auxMoverDer(i, j + 1);
				seMovieron = true;
			} else {
				if ((tablero[i][j].getValor() == tablero[i][j + 1].getValor())
						&& (tablero[i][j + 1].isCorrido() == false)) {
					tablero[i][j + 1].setCorrido(true);
					sumarPuntaje(tablero[i][j], tablero[i][j + 1]);
					moverValor(tablero[i][j], tablero[i][j + 1]);
					seMovieron = true;
				}
			}
		}
	}

	// PRIVADOS

	private void moverValor(Celda a, Celda b) {
		b.setValor(b.getValor() + a.getValor());
		a.setValor(0);
	}

	private void sumarPuntaje(Celda a, Celda b) {
		puntaje.sumarPuntaje(a.getValor() * 2);
	}

}
