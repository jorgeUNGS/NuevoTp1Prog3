package Negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class TableroMejoradoTest {

	@Test
	public void test() {
		Tablero t = new TableroMejorado(4);
		
		assertTrue(t.getCantColumnas() == 4);
		assertTrue(t.getCantCeldas() == 16);
		assertTrue(t.getCelda(0, 0).getValor() == 0);
		assertFalse(t.getCelda(3, 3).getValor() != 0);
		
		t.setCelda(1, 1, 2);
		
		assertTrue(t.getCelda(1, 1).getValor() == 2);
		
		t.setCelda(1, 1, 0);
		
		assertTrue(t.getCelda(1, 1).getValor() == 0);
		
	}

}
